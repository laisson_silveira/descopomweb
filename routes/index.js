var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect("/md-layout1/user-login-v2.html");
});

module.exports = router;
